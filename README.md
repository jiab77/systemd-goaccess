# systemd-goaccess

Creates goaccess service with SSL support using ubuntu systemd

Adapted from the work found here - http://blog.sudoask.com/run-goaccess-as-service-use-systemd/

# Installation

Just set `install.sh` as executable like this :

```shell
chmod -v +x install.sh
./install.sh
```
